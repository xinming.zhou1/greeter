package org.example.greeter

import io.micrometer.observation.Observation
import io.micrometer.observation.ObservationHandler
import io.micrometer.observation.ObservationTextPublisher
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class Application {
	private val log = LoggerFactory.getLogger(Application::class.java)

	@Bean
	fun observationTextPublisher(): ObservationHandler<Observation.Context> {
		return ObservationTextPublisher { msg -> log.info(msg) }
	}
}

fun main(args: Array<String>) {
	runApplication<Application>(*args)
}
