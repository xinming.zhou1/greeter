package org.example.greeter

import net.devh.boot.grpc.server.service.GrpcService
import org.example.greeter.proto.GreeterGrpcKt
import org.example.greeter.proto.HelloReply
import org.example.greeter.proto.HelloRequest

@GrpcService
class GreeterService : GreeterGrpcKt.GreeterCoroutineImplBase() {

    override suspend fun sayHello(request: HelloRequest): HelloReply {
        return HelloReply.newBuilder().setMessage("Hello ${request.name}!").build()
    }
}
