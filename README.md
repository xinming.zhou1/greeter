# Grpc String Boot Template

This template bootstraps the following libraries
* Spring Boot 3.2.4
* String Boot GRPC Starter 3.1.0.RELEASE
* Gradle 8.7